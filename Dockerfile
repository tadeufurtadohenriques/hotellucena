FROM maven:3-jdk-8

WORKDIR /app

COPY . /app

EXPOSE 8080

CMD ["java", "-jar", "0.0.0.:8080"]